#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"

import browser
from browser import (
	console,
	document as root_view,
	html,
)

from helpers.logging import logging
from models.abstract import ObservableAttrDict


class DataModel(ObservableAttrDict):
	"""Provides transparent access to remote data (and its future updates if connection is bidirectional)"""
	def __init__(self, url):
		super(DataModel, self).__init__()

		if url.startswith('http://') or url.startswith('https://'):
			from helpers.rest import REST
			self._remote_model = REST(url, received=self._received)

		elif url.startswith('ws://') or url.startswith('wss://'):
			from helpers.websocket import WebSocket
			self._remote_model = WebSocket(url, received=self._received)

	def __getitem__(self, key):
		if not hasattr(self, key):
			if key.startswith('__') and key.endswith('__'):
				pass
			else:
				logging.getLogger(__name__).info('Data model has no key %s, fetching', key)
				self._remote_model.send(key)

				# Sets default value (till not fetched)
				self.setdefault(key, None)
		return super(DataModel, self).__getitem__(key)

	def _received(self, data):
		# logging.getLogger(__name__).info('data=' + '%s', data)
		self[data['key']] = data['value']
