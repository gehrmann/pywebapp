#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"

from helpers.javascript import LocalStorageMemento
from helpers.logging import logging
from models.abstract import ObservableAttrDict


class SettingsModel(ObservableAttrDict):
	"""Keeps user settings"""
	def __init__(self, memento=None):
		super(SettingsModel, self).__init__()

		# Protects from saving default values
		self.__dict__['defaults'] = ObservableAttrDict(
			# ...
			# (insert default settings here)
			# ...
		)

		if memento is not None:
			self._memento = memento
			self.update(memento.restore())
			self.changed.bind(memento.save)

		self.defaults.changed.bind(self._on_model_updated)

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		logging.getLogger(__name__).info('_on_model_updated(): %s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])

		if model is self.defaults:
			if current[0] is not None:
				self.update(current[0])

	def __repr__(self):
		return object.__repr__(self)

	def __getitem__(self, key):
		try:
			value = super(SettingsModel, self).__getitem__(key)
		except KeyError:
			value = self.defaults.get(key)
		return value
