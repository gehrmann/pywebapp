#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"

timer.stop('time.py.tmp3')
from helpers.logging import logging
from helpers.observable import observable


class AttrDict(dict):
	"""Same as the dict, but values are through "." (as attributes) accessible"""

	__getattr__ = dict.__getitem__
	__setattr__ = dict.__setitem__
	__delattr__ = dict.__delitem__

	def __getstate__(self):
		return self.__dict__


class _NestedObservablesMixin(object):
	"""Common mix-in for observable objects"""
	def __init__(self):
		self.__dict__['_items_to_callbacks_to_unbind'] = {}  # Hides from repr and __setattr__/__setitem__

	def _unbind_changed(self, values):
		"""Unbinds changed-event if values-item is instance of Observable*, returns values.

		Needed to unbind bound changed-events of item (from values) if it is being removed from item-holder (self).
		"""
		for value in (values.values() if isinstance(self, ObservableDict) else values):
			if isinstance(value, (ObservableDict, ObservableList, ObservableSet)):
				value.changed.unbind(self._items_to_callbacks_to_unbind.pop(id(value)))
		return values

	def __repr__(self):
		return object.__repr__(self)


class ObservableDict(dict, _NestedObservablesMixin):
	"""Dict, which implements pattern Observer-Observable"""

	@observable
	@classmethod
	def changed(cls, item=None, previous=(None, ), current=(None, )):
		"""Bind to it to receive 'changed'-events: (object.changed.bind(handler)), call it to send 'changed'-event"""

	def __init__(self, *args, **kwargs):
		_NestedObservablesMixin.__init__(self)
		super(ObservableDict, self).__init__(*args, **kwargs)

		previous, current = (self._unbind_changed({k: None for k in kwargs}), ), (self._bind_changed({k: v for k, v in kwargs.items()}), )
		self.changed(self, previous=previous, current=current)

	def __hash__(self):
		return id(self)
		# return hash(frozenset((repr(x) for x in self.items())))

	def __setitem__(self, key, value):
		# Checks if there is a property with the same name -> calls it's setter
		if key.__class__ is str and hasattr(self.__class__, key) and getattr(self.__class__, key).__class__ is property:
			previous = (self._unbind_changed({key: getattr(self.__class__, key).fget(self)}), )
			getattr(self.__class__, key).fset(self, value)  # Call property's setter
			current = (self._bind_changed({key: getattr(self.__class__, key).fget(self)}), )
			self.changed(self, previous=previous, current=current)

		elif key not in self or self[key] != value:
			previous, current = (self._unbind_changed({key: self.get(key, None)}), ), (self._bind_changed({key: value}), )
			super(ObservableDict, self).__setitem__(key, value)
			self.changed(self, previous=previous, current=current)

	__setattr__ = __setitem__

	def __delitem__(self, key):
		previous, current = (self._unbind_changed({key: self.get(key, None)}), ), (self._bind_changed({key: None}), )
		super(ObservableDict, self).__delitem__(key)
		self.changed(self, previous=previous, current=current)

	__delattr__ = __delitem__

	def clear(self):
		previous, current = (self._unbind_changed({k: v for k, v in self.items()}), ), (self._bind_changed({k: None for k in self}), )
		super(ObservableDict, self).clear()
		if current:
			self.changed(self, previous=previous, current=current)

	def pop(self, key, *args):
		previous, current = (self._unbind_changed({key: self.get(key, None)}), ), (self._bind_changed({key: None}), )
		value = super(ObservableDict, self).pop(key, *args)
		if previous != current:
			self.changed(self, previous=previous, current=current)
		return value

	def popitem(self):
		key, value = item = super(ObservableDict, self).popitem()
		previous, current = (self._unbind_changed({key: value}), ), (self._bind_changed({key: None}), )
		self.changed(self, previous=previous, current=current)
		return item

	def setdefault(self, key, *args):
		"""Dict.setdefault with lazy-calculated default value"""

		previous = (self._unbind_changed({key: self.get(key, None)}), )
		value = super(ObservableDict, self).setdefault(key, *([x() for x in args] if key not in self and len(args) > 0 and callable(args[0]) else args))
		current = (self._bind_changed({key: value}), )
		if previous != current:
			self.changed(self, previous=previous, current=current)
		return value

	def update(self, items, **kwargs):
		kwargs.update(dict(items))

		updated_keys = {k for k, v in kwargs.items() if k not in self or self[k] != v}
		if updated_keys:
			previous = (self._unbind_changed({k: self.get(k, None) for k in updated_keys}), )
			super(ObservableDict, self).update(kwargs)
			current = (self._bind_changed({k: self[k] for k in updated_keys}), )
			self.changed(self, previous=previous, current=current)

	def clear_update(self, items, **kwargs):
		kwargs.update(dict(items))

		updated_keys = {k for k in [self.keys()] + [kwargs.keys()] if self.get(k, None) != kwargs.get(k, None)}
		if updated_keys:
			previous = (self._unbind_changed({k: self.get(k, None) for k in updated_keys}), )
			super(ObservableDict, self).update(kwargs)
			current = (self._bind_changed({k: self[k] for k in updated_keys}), )
			self.changed(self, previous=previous, current=current)

	"""Helpers"""

	def _bind_changed(self, values):
		"""Binds changed-event if values-item is instance of Observable*, returns values.

		Needed to provide changed-events of item (from values) up to item-holder (self).
		"""
		for key, value in values.items():
			if isinstance(value, (ObservableDict, ObservableList, ObservableSet)):
				value.changed.bind(self._items_to_callbacks_to_unbind.setdefault(id(value), (lambda key, value: lambda model=None, previous=(None, ), current=(None, ): (
					self.changed(self, previous=({key: value}, ) + previous, current=({key: value}, ) + current)
				))(key, value)))  # invoke parent's changed-event if is changed
				# def _get_callback(key, value):
				#     def _callback(model=None, previous=(None, ), current=(None, )):
				#         return self.changed(self, previous=({key: value}, ) + previous, current=({key: value}, ) + current)
				#     return _callback
				# value.changed.bind(self._items_to_callbacks_to_unbind.setdefault(id(value), _get_callback(key, value)))  # invoke parent's changed-event if is changed

		return values


class ObservableAttrDict(ObservableDict, AttrDict):
	pass


class ObservableList(list, _NestedObservablesMixin):
	"""List, which implements pattern Observer-Observable"""

	@observable
	@classmethod
	def changed(cls, item=None, previous=(None, ), current=(None, )):
		"""Bind to it to receive 'changed'-events: (object.changed.bind(handler)), call it to send 'changed'-event"""

	def __init__(self, *args):
		_NestedObservablesMixin.__init__(self)
		super(ObservableList, self).__init__(*args)
		if super(ObservableList, self).__len__():  # Prevents from calling reloaded method
			previous, current = (self._unbind_changed({(0, 0): []}), ), (self._bind_changed({(0, len(self)): list(self)}), )
			self.changed(self, previous=previous, current=current)

	def __hash__(self):
		return id(self)
		# return hash(repr(self))

	def __setitem__(self, index, value):
		if len(self) <= index or self[index] != value:
			previous = (self._unbind_changed({(index, index + 1): [self[index]]}), )
			super(ObservableList, self).__setitem__(index, value)
			current = (self._bind_changed({(index, index + 1): [self[index]]}), )
			self.changed(self, previous=previous, current=current)

	def __delitem__(self, index):
		previous = (self._unbind_changed({(index, index + 1): [self[index]]}), )
		super(ObservableList, self).__delitem__(index)
		current = (self._bind_changed({(index, index): []}), )
		self.changed(self, previous=previous, current=current)

	def __setslice__(self, from_index, to_index, values):
		to_index = min(to_index, len(self))  # to_index for [:] is a very big number
		previous = (self._unbind_changed({(from_index, to_index): self[from_index:to_index]}), )
		super(ObservableList, self).__setslice__(from_index, to_index, values)
		current = (self._bind_changed({(from_index, from_index + len(values)): values}), )
		self.changed(self, previous=previous, current=current)

	def __delslice__(self, from_index, to_index):
		previous = (self._unbind_changed({(from_index, to_index): self[from_index:to_index]}), )
		super(ObservableList, self).__delslice__(from_index, to_index)
		current = (self._bind_changed({(from_index, from_index): []}), )
		self.changed(self, previous=previous, current=current)

	def __iadd__(self, values):
		previous, current = (self._unbind_changed({(len(self), len(self)): []}), ), (self._bind_changed({(len(self), len(self) + len(values)): values}), )
		super(ObservableList, self).__iadd__(values)
		self.changed(self, previous=previous, current=current)
		return self

	def __imul__(self, count):
		if count != 1:
			if count < 1:
				previous, current = (self._unbind_changed({(0, len(self)): self[:]}), ), (self._bind_changed({(0, 0): []}), )
			else:
				previous, current = (self._unbind_changed({(len(self), len(self)): []}), ), (self._bind_changed({(len(self), len(self) * (count)): self[:] * (count - 1)}), )
			super(ObservableList, self).__imul__(count)
			self.changed(self, previous=previous, current=current)
		return self

	def append(self, value):
		super(ObservableList, self).append(value)
		previous, current = (self._unbind_changed({(len(self) - 1, len(self) - 1): []}), ), (self._bind_changed({(len(self) - 1, len(self)): [value]}), )
		self.changed(self, previous=previous, current=current)

	def insert(self, index, value):
		super(ObservableList, self).insert(index, value)
		index = min(index, len(self) - 1)
		previous, current = (self._unbind_changed({(index, index): []}), ), (self._bind_changed({(index, index + 1): [value]}), )
		self.changed(self, previous=previous, current=current)

	def extend(self, values):
		previous, current = (self._unbind_changed({(len(self), len(self)): []}), ), (self._bind_changed({(len(self), len(self) + len(values)): values}), )
		super(ObservableList, self).extend(values)
		self.changed(self, previous=previous, current=current)

	def pop(self, index=None):
		if index is None:
			index = len(self) - 1
		value = super(ObservableList, self).pop(index)
		previous, current = (self._unbind_changed({(index, index + 1): [value]}), ), (self._bind_changed({(index, index): []}), )
		self.changed(self, previous=previous, current=current)
		return value

	def remove(self, value):
		index = self.index(value)
		super(ObservableList, self).remove(value)
		previous, current = (self._unbind_changed({(index, index + 1): [value]}), ), (self._bind_changed({(index, index): []}), )
		self.changed(self, previous=previous, current=current)

	def reverse(self):
		previous = {(0, len(self)): self[:]}
		super(ObservableList, self).reverse()
		current = {(0, len(self)): self[:]}
		if previous != current:
			self.changed(self, current=current, previous=previous)

	def sort(self):
		previous = {(0, len(self)): self[:]}
		super(ObservableList, self).sort()
		current = {(0, len(self)): self[:]}
		if previous != current:
			self.changed(self, current=current, previous=previous)

	"""Helpers"""

	def _bind_changed(self, values):
		"""Binds changed-event if values-item is instance of Observable*, returns values.

		Needed to provide changed-events of item (from values) up to item-holder (self).
		"""
		for value in values:
			if isinstance(value, (ObservableDict, ObservableList, ObservableSet)):
				value.changed.bind(self._items_to_callbacks_to_unbind.setdefault(id(value), (lambda value: lambda model=None, previous=(None, ), current=(None, ): (
					self.changed(self, previous=({(self.index(value), self.index(value)): (value, )}, ) + previous, current=({(self.index(value), self.index(value)): (value, )}, ) + current)
				))(value)))  # invoke parent's changed-event if is changed
				# def _get_callback(value):
				#     def _callback(model=None, previous=(None, ), current=(None, )):
				#         return self.changed(self, previous=({(self.index(value), self.index(value)): (value, )}, ) + previous, current=({(self.index(value), self.index(value)): (value, )}, ) + current)
				#     return _callback
				# value.changed.bind(self._items_to_callbacks_to_unbind.setdefault(id(value), _get_callback(value)))  # invoke parent's changed-event if is changed

		return values


class ObservableSet(set, _NestedObservablesMixin):
	"""Set, which implements pattern Observer-Observable"""

	@observable
	@classmethod
	def changed(cls, item=None, previous=(None, ), current=(None, )):
		"""Bind to it to receive 'changed'-events: (object.changed.bind(handler)), call it to send 'changed'-event"""

	def __init__(self, *args):
		_NestedObservablesMixin.__init__(self)
		super(ObservableSet, self).__init__(*args)
		values = set(*args)
		previous, current = (self._unbind_changed({}), ), (self._bind_changed(set(*args)), )
		self.changed(self, previous=previous, current=current)

	def __repr__(self):
		return 'set([{1}])'.format(self, ', '.join([repr(x) for x in self]))

	def __hash__(self):
		return id(self)
		# return hash(frozenset(self))

	def __iand__(self, values):
		previous = (self._unbind_changed(set(self - values)), )
		if previous:
			super(ObservableSet, self).__iand__(values)
			current = (self._bind_changed(set()), )
			self.changed(self, previous=previous, current=current)
		return self

	def __ior__(self, values):
		values = values.difference(self)
		if values:
			super(ObservableSet, self).__ior__(values)
			previous, current = (self._unbind_changed(set()), ), (self._bind_changed(values), )
			self.changed(self, previous=previous, current=current)
		return self

	def __isub__(self, values):
		values = values & self
		if values:
			super(ObservableSet, self).__isub__(values)
			previous, current = (self._unbind_changed(values), ), (self._bind_changed(set()), )
			self.changed(self, previous=previous, current=current)
		return self

	def __ixor__(self, values):
		previous, current = (self._unbind_changed(values & self), ), (self._bind_changed(values.difference(self)), )
		super(ObservableSet, self).__ixor__(values)
		self.changed(self, previous=previous, current=current)
		return self

	def add(self, value):
		if value not in self:
			super(ObservableSet, self).add(value)
			previous, current = (self._unbind_changed(set()), ), (self._bind_changed(set([value])), )
			self.changed(self, previous=previous, current=current)

	def clear(self):
		previous, current = (self._unbind_changed(set(self)), ), (self._bind_changed(set()), )
		if previous:
			super(ObservableSet, self).clear()
			self.changed(self, previous=previous, current=current)

	def difference_update(self, values):
		previous, current = (self._unbind_changed(values & self), ), (self._bind_changed(set()), )
		if previous:
			super(ObservableSet, self).difference_update(values)
			self.changed(self, previous=previous, current=current)

	def discard(self, value):
		if value in self:
			super(ObservableSet, self).discard(value)
			previous, current = (self._unbind_changed(set([value])), ), (self._bind_changed(set()), )
			self.changed(self, previous=previous, current=current)

	def intersection_update(self, values):
		previous, current = (self._unbind_changed(set(self - values)), ), (self._bind_changed(set()), )
		if previous:
			super(ObservableSet, self).intersection_update(values)
			self.changed(self, previous=previous, current=current)

	def pop(self):
		value = super(ObservableSet, self).pop()
		previous, current = (self._unbind_changed(set([value])), ), (self._bind_changed(set()), )
		self.changed(self, previous=previous, current=current)
		return value

	def remove(self, value):
		super(ObservableSet, self).remove(value)
		previous, current = (self._unbind_changed(set([value])), ), (self._bind_changed(set()), )
		self.changed(self, previous=previous, current=current)

	def symmetric_difference_update(self, values):
		previous, current = (self._unbind_changed(values & self), ), (self._bind_changed(values - self), )
		super(ObservableSet, self).symmetric_difference_update(values)
		self.changed(self, previous=previous, current=current)

	def update(self, values):
		if values:
			previous, current = (self._unbind_changed(set()), ), (self._bind_changed(values - self), )
			value = super(ObservableSet, self).update(values)
			self.changed(self, previous=previous, current=current)

	def clear_update(self, values):
		if self or values:
			previous, current = (self._unbind_changed(self - values), ), (self._bind_changed(values - self), )
			super(ObservableSet, self).clear()
			value = super(ObservableSet, self).update(values)
			self.changed(self, previous=previous, current=current)

	"""Helpers"""

	def _bind_changed(self, values):
		"""Binds changed-event if values-item is instance of Observable*, returns values.

		Needed to provide changed-events of item (from values) up to item-holder (self).
		"""
		for value in values:
			if isinstance(value, (ObservableDict, ObservableList, ObservableSet)):
				value.changed.bind(self._items_to_callbacks_to_unbind.setdefault(id(value), (lambda value: lambda model=None, previous=(None, ), current=(None, ): (
					self.changed(self, previous=(value, ) + previous, current=(value, ) + current)
				))(value)))  # invoke parent's changed-event if is changed
				# def _get_callback(value):
				#     def _callback(model=None, previous=(None, ), current=(None, )):
				#         return self.changed(self, previous=(value, ) + previous, current=(value, ) + current)
				#     return _callback
				# value.changed.bind(self._items_to_callbacks_to_unbind.setdefault(id(value), _get_callback(value)))  # invoke parent's changed-event if is changed

		return values
