#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"

from browser import (
	console,
	document as root_view,
	html,
)

from helpers.javascript import Location
from helpers.logging import logging
from helpers.timer import timer
timer.start('time.py.tmp3')
from models.abstract import ObservableAttrDict, ObservableSet


class StateModel(ObservableAttrDict):
	"""Keeps information about local state (bidirectional synchronization with URI)"""

	def __init__(self):
		super(StateModel, self).__init__()

		self.controllers = ObservableSet()

		self.changed.bind(self._on_model_updated)
		Location.bind(name='uri', path='*notFound', changed=self._on_model_updated)

	"""Model's event handlers"""

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).info('_on_model_updated(): %s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])
		state_model = self

		if model is state_model:
			# Updates location by state_model
			Location.set(''.join('/{}={}'.format(k, (','.join(v) if isinstance(v, set) else v)) for k, v in state_model.items() if v is not None))

		if model is Location:
			# Updates state_model by location
			state = {k: v for x in (current[0] or '').split('/') if x for k, v in [x.split('=')]}

			# Converts list of controllers (comma-separated) into set with default value
			controllers = {x for x in state.pop('controllers', '').split(',') if x}
			state_model.controllers.clear_update(controllers)

		# timer.timestamp('View was updated by model')
