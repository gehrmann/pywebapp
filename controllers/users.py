#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"

from browser import (
	console,
	document as root_view,
	html,
)
jQuery = browser.window.jQuery

from helpers.logging import logging
from helpers.template import Template
from controllers.abstract import AbstractController


class UsersController(AbstractController):
	view_id = 'users'

	"""Model's event handlers"""
	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		logging.getLogger(__name__).info('_on_model_updated(): %s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])
		view = self._view
		state_model = self._state_model
		data_model = self._data_model
		settings_model = self._settings_model

		if model is data_model:
			if current[0] is None or 'users' in current[0] and previous[0] != current[0]:
				# logging.getLogger(__name__).info('Users: %s', (data_model['users'] or {}).get('_items', []))
				data_model['users']  # Initiates lazy loading (because it is not supported in template)
				view.innerHTML = Template(
					html='''
						<div class="btn-group" style="width: 201px">
							<ul class="list-group">
								<a class="list-group-item active">
									<h4 class="list-group-item-heading">Users</h4>
									{{ /* <p class="list-group-item-text">List of registered users</p> */ }}
								</a>
								{{ if (!data_model.users) { }}
									Loading...
								{{ } else if (!data_model.users._items) { }}
									No users
								{{ } else { }}
									{{ _.each(data_model.users._items, function(item) { }}
										<li class="list-group-item">
											{{= item.firstname }} {{= item.lastname }} ({{= item.username }})
											<br/>
											<small>Created: {{= item._created }}</small>
											<br/>
											<small>Updated: {{= item._updated }}</small>
											<br/>
											<a>link</a>
											<a href="link">link</a>
											<a target="remove">remove</a>
										</li>
									{{ }); }}
								{{ }; }}
							</ul>
						</div>
					''',
				)(**locals())
				self._bind_links(view)

	def _on_remove_link_clicked(self, event):
		# self.hide()
		self._state_model.controllers.remove('users')
