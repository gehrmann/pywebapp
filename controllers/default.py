#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"

import browser
jQuery = browser.window.jQuery

from helpers.logging import logging
from helpers.template import Template
from controllers.abstract import AbstractController


class DefaultController(AbstractController):
	view_id = 'default'

	"""Model's event handlers"""

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).info('_on_model_updated(): %s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])
		view = self._view
		state_model = self._state_model
		data_model = self._data_model
		settings_model = self._settings_model

		if model is data_model:
			# if current[0] is None or 'users' in current[0] and previous[0] != current[0]:
				view.innerHTML = Template(
					html='''
						No controller selected
					''',
				)(**locals())

	def show(self):
		jQuery(self._view).show()

	def hide(self):
		jQuery(self._view).hide()
