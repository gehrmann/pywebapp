#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"

from browser import (
	console,
	document as root_view,
	html,
)

from helpers.logging import logging


class AbstractController(object):
	view_id = None
	view_selector = None

	def __init__(self, view, state_model, data_model, settings_model):
		self._view = view[self.view_id] if self.view_id is not None else (view.get(selector=self.view_selector) if self.view_selector is not None else view)
		# self._$view = jQuery(self._view)

		self._state_model = state_model
		self._data_model = data_model
		self._settings_model = settings_model

		# Observes models
		state_model.changed.bind(self._on_model_updated)
		data_model.changed.bind(self._on_model_updated)
		settings_model.changed.bind(self._on_model_updated)

		# Fills by models
		self._on_model_updated(data_model)
		self._on_model_updated(settings_model)
		self._on_model_updated(state_model)

	"""Helpers"""

	def _bind_links(self, view):
		for link in view.get(selector='a[target]'):
			link.bind('click', getattr(self, '_on_{0.target}_link_clicked'.format(link)))

	def show(self):
		jQuery(self._view).hide().fadeIn()

	def hide(self):
		jQuery(self._view).fadeOut()
