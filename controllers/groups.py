#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"

from helpers.logging import logging
from helpers.template import Template
from controllers.abstract import AbstractController


class GroupsController(AbstractController):
	view_id = 'groups'

	"""Model's event handlers"""
	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		# logging.getLogger(__name__).info('_on_model_updated(): %s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])
		view = self._view
		state_model = self._state_model
		data_model = self._data_model
		settings_model = self._settings_model

		if model is data_model:
			if current[0] is None or 'groups' in current[0] and previous[0] != current[0]:
				# logging.getLogger(__name__).info('groups: %s', (data_model['groups'] or {}).get('_items', []))
				data_model['groups']  # Initiates lazy loading (because it is not supported in template)
				view.innerHTML = Template(
					html='''
						<div class="btn-group" style="width: 201px">
							<ul class="list-group">
								<a href="#" class="list-group-item active">
									<h4 class="list-group-item-heading">Groups</h4>
									<!-- p class="list-group-item-text">List of available groups</p -->
								</a>
								{{ if (!data_model.groups) { }}
									Loading...
								{{ } else if (!data_model.groups._items) { }}
									No groups
								{{ } else { }}
									{{ _.each(data_model.groups._items, function(item) { }}
										<li class="list-group-item">{{= item.title }}</li>
									{{ }); }}
								{{ }; }}
							</ul>
						</div>
					''',
				)(**locals())
				self._bind_links(view)
