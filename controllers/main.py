#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"

from browser import (
	console,
	document as root_view,
	html,
)
jQuery = browser.window.jQuery

from helpers.logging import logging
from helpers.template import Template
from controllers.abstract import AbstractController


class MainController(AbstractController):
	view_id = 'main'

	"""Model's event handlers"""
	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		logging.getLogger(__name__).info('_on_model_updated(): %s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])
		view = self._view
		state_model = self._state_model
		data_model = self._data_model
		settings_model = self._settings_model

		if model is state_model:
			# logging.getLogger(__name__).info('previous[0], current[0]=' + '%s, %s', previous[0], current[0])
			if current[0] is None or 'controllers' in current[0] and previous[:2] != current[:2]:
				# Updates selectors
				# logging.getLogger(__name__).info('--------------------------------------')
				# logging.getLogger(__name__).info('state_model.controllers=' + '%s', state_model.controllers)
				# logging.getLogger(__name__).info('"users" in state_model.controllers=' + '%s', "users" in state_model.controllers)
				getattr(jQuery('a[target="show_users"]', self._view).parent(), 'addClass' if 'users' in state_model.controllers else 'removeClass')('active')
				getattr(jQuery('a[target="show_groups"]', self._view).parent(), 'addClass' if 'groups' in state_model.controllers else 'removeClass')('active')

		if model is data_model:
			if current[0] is None:
				view.innerHTML = Template(
					html='''
						<ul class="nav nav-pills">
							<li role="presentation"><a target="show_users">Users</a></li>
							<li role="presentation"><a target="show_groups">Groups</a></li>
						</ul>
					''',
				)(**locals())
				self._bind_links(view)

	def _on_show_users_link_clicked(self, event):
		(self._state_model.controllers.add if 'users' not in self._state_model.controllers else self._state_model.controllers.remove)('users')

	def _on_show_groups_link_clicked(self, event):
		(self._state_model.controllers.add if 'groups' not in self._state_model.controllers else self._state_model.controllers.remove)('groups')
