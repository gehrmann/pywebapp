#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"

__doc__ = """

Attention!
==========

Brython parses python code slowly! Measure it between calling import and starting module execution.
Script works well only through web-server! Otherwise AJAX is broken and nobody sends content type in headers.
Do not import modules! They are partly in "brython_stdlib.js" and loading consumes performance.
Do not use __getattr__. It is *very* slow!


"""

import _sys
import browser
from browser import (
	console,
	document as root_view,
	html,
)
console.timeEnd('time.brython.init')  # Early stopping before helpers.timer is loaded
console.time('time.py.import')  # Early starting before helpers.timer is loaded
brython = browser.window.__BRYTHON__
jQuery = browser.window.jQuery
# import javascript
# import sys  # From brython_stdlib.js

from helpers.javascript import Location
from helpers.logging import logging
from helpers.timer import timer
from models.state import StateModel
from models.data import DataModel
from models.settings import SettingsModel

timer.stop('time.py.import')
timer.start('time.py.init')

# document <= html.LINK(rel="stylesheet", href=_path+'css/smoothness/jquery-ui.css')
# browser.load(_path+'jquery-1.11.2.min.js', ['jQuery'])


def _main():
	timer.start('time.py.main')

	timer.start('time.py.main.models')
	state_model = StateModel()
	data_model = DataModel(
		# url='http://0.0.0.0:5000',  # REST
		url='ws://0.0.0.0:8000',  # WebSocket
	)
	settings_model = SettingsModel(
		# memento=LocalStorageMemento(key='settings'),
	)
	timer.stop('time.py.main.models')

	def on_model_updated(model=None, previous=(None, ), current=(None, ), __state=dict(controllers=dict(), default_controller=None)):
		# logging.getLogger(__name__).info('on_model_updated(): %s %s %s', model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])

		if model is state_model:
			# Loads/Unloads controllers by state_model
			if current[0] is None or 'controllers' in current[0] and previous[:2] != current[:2]:
				try:
					# Unloads loaded controllers
					for name in (previous[1] if previous[0] is not None else []):
						controller = __state['controllers'].pop(name)
						getattr(controller, 'hide', lambda: ())()

					# Loads awaiting controllers
					for name, class_name in [(x, '{}Controller'.format(x.title().replace('_', ''))) for x in (current[0] and current[1] or [])]:
						timer.start('time.py.main.controllers.' + name + '.import')
						controller_module = __import__('controllers.{name}'.format(**locals()), globals(), locals(), [class_name])
						timer.stop('time.py.main.controllers.' + name + '.import')
						timer.start('time.py.main.controllers.' + name + '.init')
						__state['controllers'][name] = controller = getattr(controller_module, class_name)(
							view=root_view,
							state_model=state_model,
							data_model=data_model,
							settings_model=settings_model,
						)
						getattr(controller, 'show', lambda: ())()
						timer.stop('time.py.main.controllers.' + name + '.init')

				finally:
					# Loads/unloads default controller (if no controller selected)
					if not state_model.controllers and __state['default_controller'] is None:
						import controllers.default
						__state['default_controller'] = controller = controllers.default.DefaultController(
							view=root_view,
							state_model=state_model,
							data_model=data_model,
							settings_model=settings_model,
						)
						getattr(controller, 'show', lambda: ())()
					elif state_model.controllers and __state['default_controller'] is not None:
						controller, __state['default_controller'] = __state['default_controller'], None
						getattr(controller, 'hide', lambda: ())()
	state_model.changed.bind(on_model_updated)
	on_model_updated(state_model)

	timer.start('time.py.main.router')
	Location.start()  # Starts router (loads controllers)
	timer.stop('time.py.main.router')

	timer.stop('time.py.main')
	timer.stop('time.init')

if __name__ == '__main__':
	timer.stop('time.py.init')
	_main()
