console.time('time.init')  // The first one
console.time('time.js.init')

// Customizes symbols for underscore template
_.templateSettings = {
	evaluate: /\{\{(.+?)\}\}/g,  // Executes javascript code
	interpolate: /\{\{-(.+?)\}\}/g,  // Does not escape html special chars
	escape: /\{\{=(.+?)\}\}/g,  // Escapes html special chars
}

$(function() {

	// $.getScript('../scripts/bootstrap.min.js'); $('<link rel="stylesheet" type="text/css" href="../styles/bootstrap.min.css">').appendTo('head')
	// $.getScript('../scripts/bootstrap.min.js'); $('<link rel="stylesheet" type="text/css" href="../styles/bootstrap.min.css">').appendTo('head')
	// $.getScript('scripts/main.py')
	// $('<script type="text/python" src="scripts/main.py"></script>').appendTo('head')

	console.timeEnd('time.js.beforeOnDocumentReady')
})

console.timeEnd('time.js.init')
console.time('time.html.beforeOnLoad')
console.time('time.js.beforeOnDocumentReady')
