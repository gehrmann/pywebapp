#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"



def inline_callbacks(function):
	"""Modifies generator to simplify conversion from callback-aided to consecutive description"""
	def wrapper(*args, **kwargs):

		# Creates generator on the fly
		def _generator():
			generator = yield

			# Launches wrapped function (till first 'yield')
			wrapped_generator = function(generator, *args, **kwargs)
			wrapped_generator.send(None)

			while True:
				# Waits till function (from yield statement) sends result (through generator.send(...))
				result = yield generator

				# Returns result into wrapped function (through yield-statement)
				wrapped_generator.send(result)

		# Launches generator and sends itself to it
		generator = _generator()
		generator.send(None)
		generator.send(generator)

	return wrapper


def callback_to_generator(generator):
	"""Part of inline_callbacks"""
	def _callback_to_generator(*args):
		try:
			# Returns result into generator (through yield-statement)
			generator.send(args)
		except StopIteration:
			pass
	return _callback_to_generator
