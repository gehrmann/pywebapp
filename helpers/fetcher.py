#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"

from _sys import Getframe as sys_getframe
import browser
from browser import (
	console,
)
jQuery = browser.window.jQuery

from helpers.inline_callbacks import callback_to_generator
from helpers.javascript import (
	dict_to_js,
	js_to_dicts,
)
from helpers.logging import logging
from helpers.timer import timer


def fetch(generator=None, url=None, completed=None, **kwargs):
	# kwargs['rand'] = browser.window.Math.random()

	if logging.getLogger(__name__).level in (logging.DEBUG, logging.INFO):
		timer_key = ':{0.f_back.f_lineno} '.format(sys_getframe(1)) + '[INFO]  JSON Request [url=' + url + '?' + '&'.join('{k}={v}'.format(**locals()) for k, v in kwargs.items()) + ']'
		timer.start(timer_key)

	if generator is not None:
		# Creates wrapper that returns result into generator (through yield-statement)
		completed = callback_to_generator(generator)

	def _completed(_data, status, _response):
		data, response = js_to_dicts(_data), dict(_response)
		if logging.getLogger(__name__).level in (logging.DEBUG, logging.INFO):
			timer.stop(timer_key)
			logging.getLogger(__name__).debug('%s', data)
		completed(status, response, data)

	jQuery.getJSON(url, kwargs, _completed)
