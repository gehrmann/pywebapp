#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"

import browser
Backbone = browser.window.Backbone
jQuery = browser.window.jQuery
json = browser.window.JSON


browser.window.dump = lambda x: ('<pre>' + browser.window.JSON.stringify(x, None, '\t') + '</pre>')  # Usage in template: {{- json(x) }}


def js_to_dicts(src):
	"""Converts (recursively) javascript objects to python AttrDict objects"""
	if not hasattr(src, '__class__') or src.__class__.__name__ == 'JSObject':
		dst = dict({k: js_to_dicts(getattr(src, k)) for k in dir(src)})
	elif isinstance(src, list):
		dst = [js_to_dicts(x) for x in src]
	elif isinstance(src, dict):
		dst = dict({k: js_to_dicts(v) for k, v in src.items()})
	else:
		dst = src

	return dst


def dict_to_js(src):
	"""Converts python objects to javascript objects"""
	# dst = jQuery(dict(src))[0]
	dst = jQuery({k: (dict_to_js(v) if isinstance(v, dict) else v) for k, v in src.items()})[0]
	return dst


class Location(object):
	"""Class to operate with window.location (through backbone Router)"""
	router = Backbone.Router.new()

	@classmethod
	def start(cls):
		"""Initializes URL Router"""
		Backbone.history.start()

	@classmethod
	def set(cls, path, **kwargs):
		"""
		Arguments:
			trigger -- (bool) invoke callbacks (bound to path)
			replace -- (bool) replaces path in history (instead of add)
		"""
		# kwargs.setdefault('replace', True)
		cls.router.navigate(path, jQuery(kwargs)[0])

	@classmethod
	def bind(cls, name, path, changed):
		def _changed(*args, **kwargs):
			changed(model=cls, previous=[], current=args)
		cls.router.route(path, name, _changed)


class LocalStorageMemento(object):
	"""Implements pattern Memento (saves/restores object into/from a local storage)"""
	def __init__(self, key):
		self._key = key

	def restore(self):
		"""Restores value from javascript local storage"""
		serialized_data = browser.window.localStorage.getItem(self._key) or '{}'
		data = json.parse(serialized_data)
		return data

	def save(self, model, *args, **kwargs):
		"""Saves value into javascript local storage"""
		data = {k: v for k, v in model.items() if v is not self}
		serialized_data = json.stringify(data)
		browser.window.localStorage.setItem(self._key, serialized_data)
