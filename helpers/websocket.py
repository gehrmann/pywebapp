#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"

import browser
from browser import (
	console,
	document as root_view,
	html,
)
json = browser.window.JSON

from helpers.javascript import (
	# dict_to_js,
	js_to_dicts,
)
from helpers.logging import logging


class WebSocket(object):
	def __init__(self, url, opened=None, closed=None, received=None, failed=None):
		self.opened = opened
		self.closed = closed
		self.received = received
		self.failed = failed

		self._socket = socket = browser.window.WebSocket.new(url)
		socket.onopen = self._opened
		socket.onclose = self._closed
		socket.onmessage = self._received
		socket.onerror = self._failed

	def _opened(self, event):
		logging.getLogger(__name__).info('WebSocket: opened'); console.info(event)

	def _closed(self, event):
		logging.getLogger(__name__).info('WebSocket: closed'); console.info(event)

	def _failed(self, event):
		logging.getLogger(__name__).info('WebSocket: failed'); console.info(event)

	def _received(self, event):
		logging.getLogger(__name__).info('WebSocket: received'); console.info(event)
		# console.info(event.data)
		if self.received is not None:
			data = js_to_dicts(json.parse(event.data))
			# logging.getLogger(__name__).info('data.__class__=' + '%s', data.__class__)
			self.received(data)

	def send(self, data):
		self._socket.send(data)
