#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"

from _sys import Getframe as sys_getframe
from browser import (
	console,
	document as root_view,
	html,
)

from helpers.logging import logging


class timer(object):
	"""Wrapper for console.time(...) and console.timeEnd(...)"""

	@classmethod
	def start(cls, name):
		if logging.default_level <= logging.DEBUG:
			console.time(name)

	@classmethod
	def stop(cls, name):
		if logging.default_level <= logging.DEBUG:
			console.timeEnd(name)

	@classmethod
	def timestamp(cls, name):
		if logging.default_level <= logging.DEBUG:
			console.timeStamp(name)
