#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"

from helpers.inline_callbacks import callback_to_generator


def call_once_after(generator=None, function=None, delay=0.):
	"""Calls function once after delay (in s.)

	Can be used as a co-routine:
		>>> yield call_once_after(generator, delay=1.)
	"""
	if generator is not None:
		# Creates wrapper that returns result into generator (through yield-statement)
		function = callback_to_generator(generator)

	browser.window.setTimeout(function, delay * 1000)
