#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"



class observable(object):
	"""Decorator, implements pattern "observable - observers"

	Warning: removed weakrefs and hashable types!
	"""

	def __init__(self, function):
		self.__doc__ = function.__func__.__doc__ if hasattr(function, '__func__') else function.__doc__

		self._function = function
		self._descriptors = dict()

	def __repr__(self):
		return '<{self.__class__.__module__}.{self.__class__.__name__} from {self._function} at 0x{address:x}>'.format(self=self, address=id(self))

	def __get__(self, im_self, im_class):
		im_item = im_self if im_self is not None else im_class
		if im_item not in self._descriptors:
			self._descriptors[im_item] = self._Observable(self, im_self, im_class)
		return self._descriptors[im_item]

	class _Observable(object):
		"""Bound Observable, sends events to observers if decorated function/method is called"""

		def __init__(self, observable, im_self, im_class):
			self._observable = observable
			self._im_class = im_class
			self._im_self = im_self
			self._handlers = list()
			self._handlers_to_signals = dict()

		def bind(self, handler):
			if handler not in self._handlers:
				self._handlers.append(handler)

		def unbind(self, handler):
			self._handlers.remove(handler)

		def __call__(self, *args, **kwargs):
			im_class = self._im_class
			im_self = self._im_self

			result = self._observable._function(im_self, *args, **kwargs)

			for handler in list(self._handlers):
				try:
					handler(*args, **kwargs)
				except Exception as e:
					print('_Observable: handler=', handler)
					raise

			return result
