#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"

import browser
from browser import (
	console,
	document as root_view,
	html,
)
json = browser.window.JSON

from helpers.inline_callbacks import inline_callbacks
from helpers.javascript import (
	# dict_to_js,
	js_to_dicts,
)
from helpers.logging import logging


class REST(object):
	def __init__(self, url, opened=None, closed=None, received=None, failed=None):
		self._url = url
		self.received = received
		self.failed = failed

	@inline_callbacks
	def send(generator, self, key):
		url = '{url}/{key}'.format(url=self._url, key=key)
		logging.getLogger(__name__).debug('Fetching URL: %s', url)

		from helpers.fetcher import fetch
		_ = yield fetch(generator, url=url)
		status, response, data = _

		# logging.getLogger(__name__).info('Fetching done: status=%s, response=%s, data=%s', status, response, data)
		if status == 'success':
			logging.getLogger(__name__).info('REST: received'); console.info(data)
			if self.received is not None:
				# data = js_to_dicts(json.parse(data))
				self.received(dict(key=key, value=data))
		else:
			logging.getLogger(__name__).error('Error fetching: %s', response.statusText)
			if self.failed is not None:
				self.failed()


# class DataModel(ObservableAttrDict):
#     """Provides access to remote data"""
#     def __init__(self):
#         self.url = 'http://0.0.0.0:5000/{key}'

