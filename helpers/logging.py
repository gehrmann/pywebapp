#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"

from _sys import Getframe as sys_getframe
import browser
brython = browser.window.__BRYTHON__


class logging(object):
	"""In-line replacement for logging module"""
	DEBUG, INFO, WARNING, ERROR, FATAL = 10, 20, 30, 40, 50

	# Sets the same default logging level as for a brython.js (see html body onLoad-function)
	default_level = {0: WARNING, 1: INFO, 2: DEBUG}.get(brython.debug)

	_instances = {}

	@classmethod
	def getLogger(cls, name):
		if name not in cls._instances:
			cls._instances[name] = cls(name)
		return cls._instances[name]

	def __init__(self, name, level=None):
		self.name = name
		self.level = level or self.default_level

	def debug(self, format, *args):
		if self.default_level <= self.DEBUG:
			print('{0.f_back.f_code.co_filename}:{0.f_back.f_lineno} '.format(sys_getframe(1)) + '[DEBUG] ', format % args)

	def info(self, format, *args):
		if self.default_level <= self.INFO:
			print('{0.f_back.f_code.co_filename}:{0.f_back.f_lineno} '.format(sys_getframe()) + '[INFO] ', format % args)

	def warning(self, format, *args):
		if self.default_level <= self.WARNING:
			print('{0.f_back.f_code.co_filename}:{0.f_back.f_lineno} '.format(sys_getframe(1)) + '[WARNING] ', format % args)

	def error(self, format, *args):
		if self.default_level <= self.ERROR:
			print('{0.f_back.f_code.co_filename}:{0.f_back.f_lineno} '.format(sys_getframe(1)) + '[ERROR] ', format % args)

	def fatal(self, format, *args):
		if self.default_level <= self.FATAL:
			print('{0.f_back.f_code.co_filename}:{0.f_back.f_lineno} '.format(sys_getframe(1)) + '[FATAL] ', format % args)
