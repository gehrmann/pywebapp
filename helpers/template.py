#!/bin/sh
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
"exec" "python" "-B" "$0" "$@"

import browser
from browser import (
	console,
	document as root_view,
	html,
)

from helpers.javascript import (
	dict_to_js,
	js_to_dicts,
)


def Template(html=None, id=None, selector=None):
	"""Finds template element by id or selector, returns its rendering function (uses 'underscore').

	Template syntax:
		{{- ... }} -- print variable (raw)
		{{= ... }} -- print variable (escapes special chars)
		{{ ... }} -- javascript

	Example (by selector):
		1. Insert into your html:
			>>> jQuery('body').append('''
				<div id="groups"></div>
				<script type="text/template">
					...
				</script>
			''')

		2. Get it via:
			>>> view['groups'].innerHTML = Template(selector='#groups ~ script')(**locals())

	"""
	if id is not None:
		html = '<!-- TEMPLATE id={} -->'.format(id) + root_view[id].html + '<!-- TEMPLATE {} -->'.format(id)
	elif selector is not None:
		elements = root_view.get(selector=selector)
		if not elements:
			raise KeyError('Template with selector="{selector}" not found'.format(**locals()))
		html = '<!-- TEMPLATE selector={} -->'.format(selector) + elements[0].html + '<!-- TEMPLATE {} -->'.format(id)

	wrapper = browser.window._.template(html)

	def _wrapper(**kwargs):
		return wrapper(dict_to_js(kwargs))

	return _wrapper
